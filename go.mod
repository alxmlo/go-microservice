module gitlab.com/alxmlo/go-microservice

go 1.16

require (
	github.com/asaskevich/govalidator v0.0.0-20210307081110-f21760c49a8d
	github.com/stretchr/testify v1.7.0
)
