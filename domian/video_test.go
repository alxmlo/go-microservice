package domain_test

import (
	"testing"
	"time"
	
	"github.com/stretchr/testify/require"
	"gitlab.com/alxmlo/go-microservice/domain"
	uuid "github.com/satori/go.uuid"
)

func TestValidateIfVideoIsEmpty(t *testing.T) {
	video := domain.NewVideo()
	err := video.Validate()

	require.Error(t, err)
}

func TestVideoIdIsNotAUuid(t *testing.T) {
	video := domain.NewVideo()

	video.ID = "vid"
	video.ResourceID = "rid"
	video.FilePath = "fpath"
	video.CreatedAt = time.Now()

	err := video.Validate()
	require.Error(t, err)
}

func TestVideoValidation(t *testing.T) {
	video := domain.NewVideo()

	video.ID = uuid.NewV4().String()
	video.ResourceID = "rid"
	video.FilePath = "fpath"
	video.CreatedAt = time.Now()

	err := video.Validate()
	require.Nil(t, err)
}